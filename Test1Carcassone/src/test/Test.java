/*
 * Bevers Tim
 */
package test;

import spel.*;

public class Test {

	public static void main(String[] args) {
		Speler player1 = new Speler("Janneman");
		Speler player2 = new Speler("Robinson");

		Gebied klooster1 = new Klooster();
		klooster1.WerkAf();
		Gebied weg1 = new Weg();
		weg1.VoegTegelToe(new Tegel(Tegel.getRandomTegel()));
		weg1.VoegTegelToe(new Tegel(Tegel.getRandomTegel()));
		weg1.WerkAf();
		Gebied wei1 = new Wei();

		player1.VoegGebiedToe(klooster1);

		System.out.println(player1);
		System.out.println(player2);

		player2.VoegGebiedToe(weg1);

		System.out.println(player1);
		System.out.println(player2);

		player1.VoegGebiedToe(wei1);

		System.out.println(player1);
		System.out.println(player2);

		player2.VoegGebiedToe(weg1);

		System.out.println(player1);
		System.out.println(player2);
		
		System.out.println(player1);
		System.out.println(player1.berekenEindScore());
		System.out.println(player2);
		System.out.println(player2.berekenEindScore());
		

	}

}
