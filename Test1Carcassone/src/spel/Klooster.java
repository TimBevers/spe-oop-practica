/*
 * Bevers Tim
 */
package spel;

import java.util.*;

public class Klooster extends Gebied implements Afwerkbaar {

	@Override
	public String toString() {
		return "Klooster";
	}

	List<Tegel> grensTegels;

	public Klooster() {
		this.grensTegels = new ArrayList<Tegel>();
	}

	public Klooster(Tegel... tegels) {
		this();
		for (Tegel s : tegels) {
			VoegTegelToe(s);
		}
	}

	@Override
	public void VoegTegelToe(Tegel tegel) {
		if (getTegels().size() >= 8 || IsAfgewerkt()) {
			WerkAf();
			return;
		}
		getTegels().add(tegel);
	}

	public Klooster(List<Tegel> tegels) {
		super(tegels);
	}

	public Klooster(Tegel tegel) {
		super(tegel);
	}

	public List<Tegel> getTegels() {
		return grensTegels;
	}

	public void setTegels(List<Tegel> grensTegels) {
		this.grensTegels = grensTegels;
	}

	@Override
	public boolean IsAfgewerkt() {
		return getAfgewerkt();
	}

	@Override
	public boolean WerkAf() {
		setAfgewerkt(true);
		return true;
	}

	@Override
	public int Score() {
		return (IsAfgewerkt()) ? 9 : 0;
	}

	@Override
	public int EindScore() {
		return (!IsAfgewerkt()) ? 1 + getTegels().size() : 0;
	}

}
