/*
 * Bevers Tim
 */
package spel;

import java.util.List;

public class Stad extends Gebied implements Afwerkbaar {

	@Override
	public String toString() {
		return "Stad";
	}

	public Stad() {
		super();
	}

	public Stad(List<Tegel> tegels) {
		super(tegels);
	}

	public Stad(Tegel tegel) {
		super(tegel);
	}

	@Override
	public boolean IsAfgewerkt() {
		return getAfgewerkt();
	}

	@Override
	public boolean WerkAf() {
		setAfgewerkt(true);
		return true;
	}

	@Override
	public int Score() {
		return (IsAfgewerkt()) ? getTegels().size() * 2 : 0;
	}

	@Override
	public int EindScore() {
		// TODO Auto-generated method stub
		return 0;
	}

}
