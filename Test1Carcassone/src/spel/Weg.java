/*
 * Bevers Tim
 */
package spel;

import java.util.List;

public class Weg extends Gebied implements Afwerkbaar {
	@Override
	public String toString() {
		return "Weg";
	}

	@Override
	public boolean IsAfgewerkt() {
		return getAfgewerkt();
	}

	@Override
	public boolean WerkAf() {
		setAfgewerkt(true);
		return true;
	}

	public Weg() {
		super();
	}

	public Weg(List<Tegel> tegels) {
		super(tegels);
	}

	public Weg(Tegel tegel) {
		super(tegel);
	}

	@Override
	public int Score() {
		return (IsAfgewerkt()) ? getTegels().size() * 1 : 0;

	}

	@Override
	public int EindScore() {
		return (!IsAfgewerkt()) ? getTegels().size() * 1 : 0;
	}

}
