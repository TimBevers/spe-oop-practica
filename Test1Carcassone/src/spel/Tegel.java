/*
 * Bevers Tim
 */
package spel;

public class Tegel {

	private int typeTegel;


	public Tegel(int i) {
		setTypeTegel(i);
	}
	
	public int getTypeTegel() {
		return typeTegel;
	}

	public void setTypeTegel(int typeTegel) {
		this.typeTegel = typeTegel;
	}

	public static int getRandomTegel() {
		return ((int)( Math.random() * 8) + 1);
	}

}
