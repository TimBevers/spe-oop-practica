/*
 * Bevers Tim
 */
package spel;

import java.util.*;

public class Wei extends Gebied implements Afwerkbaar {

	@Override
	public String toString() {
		return "Wei";
	}

	List<Stad> GrensSteden;

	public Wei() {
		this.GrensSteden = new ArrayList<Stad>();
	}

	public Wei(List<Tegel> tegels) {
		super(tegels);
	}

	public Wei(Tegel tegel) {
		super(tegel);
	}

	public Wei(Stad... steden) {
		this();
		for (Stad s : steden) {
			VoegGrensStadToe(s);
		}
	}

	public void VoegGrensStadToe(Stad stad) {
		getGrensSteden().add(stad);
	}

	public List<Stad> getGrensSteden() {
		return GrensSteden;
	}

	public void setGrensSteden(List<Stad> grensSteden) {
		GrensSteden = grensSteden;
	}

	@Override
	public boolean IsAfgewerkt() {
		return getAfgewerkt();
	}

	@Override
	public boolean WerkAf() {
		setAfgewerkt(true);
		return true;
	}

	@Override
	public int Score() {
		return 0;
	}

	@Override
	public int EindScore() {
		return  (!IsAfgewerkt()) ? 1 + getGrensSteden().size() * 3 : 0;
	}

}
