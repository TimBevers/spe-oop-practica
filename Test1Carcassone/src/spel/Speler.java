/*
 * Bevers Tim
 */
package spel;

import java.util.*;

public class Speler {

	private String naam;

	private String getNaam() {
		return naam;
	}

	private void setNaam(String naam) {
		this.naam = naam;
	}

	public Speler() {
		this.gebieden = new ArrayList<Gebied>();
	}

	public Speler(String naam) {
		this();
		setNaam(naam);
		Gebied first = new Stad(new Tegel(Tegel.getRandomTegel()));
		first.WerkAf();
		getGebieden().add(first);
	}

	private List<Gebied> getGebieden() {
		return gebieden;
	}

	private List<Gebied> gebieden;

	public void VoegGebiedToe(Gebied g) {
		getGebieden().add(g);
	}

	public int berekenScore() {
		int score = 0;
		for (Gebied g : getGebieden()) {
			score += g.Score();
		}
		return score;
	}
	
	public int berekenEindScore() {
		int score = 0;
		for (Gebied g : getGebieden()) {
			score += g.Score() + g.EindScore();
		}
		return score;
	}

	@Override
	public String toString() {
		String gebiedenLijst = "";
		for (Gebied g : getGebieden()) {
			gebiedenLijst += g.toString() + ";";
		}
		return String.format("%s - %s - %s", getNaam(), gebiedenLijst,
				berekenScore());
	}
}
