/*
 * Bevers Tim
 */
package spel;

import java.util.*;

public abstract class Gebied {

	private List<Tegel> tegels;
	private boolean afgewerkt;

	Gebied() {
		this.tegels = new ArrayList<Tegel>();
		this.afgewerkt = false;
	}

	public Gebied(Tegel tegel) {
		this();
		this.getTegels().add(tegel);
	}

	public Gebied(List<Tegel> tegels) {
		this();
		setTegels(tegels);
	}

	public List<Tegel> getTegels() {
		return tegels;
	}

	public void setTegels(List<Tegel> tegels) {
		this.tegels = tegels;
	}

	public boolean isAfgewerkt() {
		return afgewerkt;
	}

	public void setAfgewerkt(boolean afgewerkt) {
		this.afgewerkt = afgewerkt;
	}

	public boolean WerkAf() {
		setAfgewerkt(true);
		return true;
	}

	public boolean getAfgewerkt() {
		return this.afgewerkt;
	}

	public void VoegTegelToe(Tegel tegel) {
		getTegels().add(tegel);
	}
	
	public abstract int Score();
	public abstract int EindScore();

}
