/*
 * Bevers Tim
 */

import java.util.Arrays;

public class SerieSchakeling {

	private Weerstand[] weerstanden;

	public SerieSchakeling() {
		weerstanden = new Weerstand[0];
	}

	public SerieSchakeling(Weerstand[] inWeerstanden) {
		setWeerstanden(inWeerstanden);
	}

	public Weerstand[] getWeerstanden() {
		return weerstanden;
	}

	public void setWeerstanden(Weerstand[] weerstanden) {
		this.weerstanden = weerstanden;
	}

	@Override
	public String toString() {
		String result = "";
		for (Weerstand i : getWeerstanden()) {
			result += i + "; ";
		}
		return "serieschakeling van "
				+ getWeerstanden().length
				+ ((getWeerstanden().length == 1) ? " weerstand: "
						: " weerstanden: ") + result;
	}

	public void voegToe(Weerstand inR) {
		Weerstand[] nieuw = Arrays.copyOf(getWeerstanden(),
				getWeerstanden().length + 1);
		nieuw[getWeerstanden().length] = inR;
		setWeerstanden(nieuw);
	}

	public float spanning(float stroom) {
		float somSpanning = 0;
		for (Weerstand i : getWeerstanden()) {
			somSpanning += i.spanning(stroom);
		}
		return somSpanning;
	}

	public float vervangningsWeerstand() {
		float som = 0;
		for (Weerstand i : getWeerstanden()) {
			som += i.getWeerstand();
		}
		return som;
	}

	public float stroom(float spanning) {
		return spanning / vervangningsWeerstand();
	}
	
}
