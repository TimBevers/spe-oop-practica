/*
 * Bevers Tim
 */
public class Weerstand {
	// private float weerstand;// in ohm
	private float geleiding;// in Mho

	public Weerstand(float val) {
		setWeerstand(val);
	}

	public void setWeerstand(float val) {
		// weerstand = val;
		geleiding = 1 / val;
	}

	public float getWeerstand() {
		// return weerstand;
		return 1 / geleiding;
	}
	
	@Override
	public String toString() {
		return "weerstand van " + getWeerstand() + " ohm";
	}

	public float spanning(float stroom) {
		return stroom * getWeerstand();
	}

	public float stroom(float spanning) {
		return spanning / getWeerstand();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Weerstand other = (Weerstand) obj;
		if (getWeerstand() != other.getWeerstand())
			return false;
		return true;
	}

}
