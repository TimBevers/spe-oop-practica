/*
 * Bevers Tim
 */

import java.util.Arrays;

public class ParallelSchakeling {

	private Weerstand[] weerstanden;

	public ParallelSchakeling() {
		weerstanden = new Weerstand[0];
	}

	public ParallelSchakeling(Weerstand[] inWeerstanden) {
		setWeerstanden(inWeerstanden);
	}

	public Weerstand[] getWeerstanden() {
		return weerstanden;
	}

	public void setWeerstanden(Weerstand[] weerstanden) {
		this.weerstanden = weerstanden;
	}

	@Override
	public String toString() {
		String result = "";
		for (Weerstand i : getWeerstanden()) {
			result += i + "; ";
		}
		return "parallelschakeling van "
				+ getWeerstanden().length
				+ ((getWeerstanden().length == 1) ? " weerstand: "
						: " weerstanden: ") + result;
	}

	public void voegToe(Weerstand inR) {
		Weerstand[] nieuw = Arrays.copyOf(getWeerstanden(),
				getWeerstanden().length + 1);
		nieuw[getWeerstanden().length] = inR;
		setWeerstanden(nieuw);
	}

	public float spanning(float stroom) {
		return stroom * vervangningsWeerstand();
	}

	public float vervangningsWeerstand() {
		float invSom = 0;
		for (Weerstand i : getWeerstanden()) {
			invSom += 1 / i.getWeerstand();
		}
		return 1 / invSom;
	}

	public float stroom(float spanning) {
		return spanning / vervangningsWeerstand();
	}

}
