public class RegelbareWeerstand extends Weerstand {

	private float min;
	private float max;

	public RegelbareWeerstand(float min, float max) {
		super(min);
		setMin(min);
		setMax(max);
	}

	public float getMin() {
		return min;
	}

	public void setMin(float min) {
		this.min = min;
	}

	public float getMax() {
		return max;
	}

	public void setMax(float max) {
		this.max = max;
	}

	public float verhoog(float fractie) {
		setWeerstand(getWeerstand() * (1 + fractie));
		if (getWeerstand() > getMax()) {
			setWeerstand(getMax());
		} else if (getWeerstand() < getMin()) {
			setWeerstand(getMin());
		}
		return getWeerstand();
	}

	@Override
	public String toString() {
		return String.format(
				"regelbare weerstand van %s Ohm (min: %s / max: %s)",
				getWeerstand(), getMin(), getMax());
	}
}
