/*
 *  Bevers Tim
 */

public class Breuk {

	private int teller;
	private int noemer;

	// Empty constructor
	public Breuk() {
	}

	// Constructor with initial values
	public Breuk(int inTeller, int inNoemer) {
		setTeller(inTeller);
		setNoemer(inNoemer);
	}

	// Setter Teller
	public void setTeller(int inTeller) {
		teller = inTeller;
	}

	// Setter Noemer
	public void setNoemer(int inNoemer) {
		noemer = (inNoemer > 0)? inNoemer : 1;
	}

	// Getter Teller
	public int getTeller() {
		return teller;
	}

	// Getter Noemer
	public int getNoemer() {
		return noemer;
	}

	// Tostring
	public String toString() {
		return getTeller() + "/" + getNoemer();
	}

	// Print Rationeel
	public float Rationeel() {
		return ((float) getTeller() / (float) getNoemer());
	}

	// Plus
	public Breuk Plus(Breuk inBreuk) {
		Breuk result = new Breuk();

		int teller1 = this.getTeller() * inBreuk.getNoemer();
		int teller2 = inBreuk.getTeller() * this.getNoemer();
		result.setNoemer(inBreuk.getNoemer() * this.getNoemer());
		result.setTeller(teller1 + teller2);

		return result;
	}

	// Vermemigvuldig
	public Breuk Vermenigvuldig(Breuk inBreuk) {
		Breuk result = new Breuk();

		result.setNoemer(inBreuk.getNoemer() * this.getNoemer());
		result.setTeller(inBreuk.getTeller() * this.getTeller());
		result.setNoemer(this.getNoemer());

		return result;
	}

	// Vermemigvuldig geheel
	public Breuk Vermenigvuldig(int inGetal) {
		Breuk result = new Breuk();

		result.setTeller(inGetal * this.getTeller());

		return result;
	}

	// Grootste gemeeschappelijke deler
	private int getGgd(int inTeller, int inNoemer) {
		int ggd = 1;
		for (int i = 1; i <= inNoemer; i++) {
			if (inTeller % i == 0 && inNoemer % i == 0) {
				ggd = i;
			}
		}
		return ggd;
	}

	// Vereenvoudig
	private void vereenvoudig() {
		int ggd = getGgd(getTeller(), getNoemer());
		setTeller(getTeller() / ggd);
		setNoemer(getNoemer() / ggd);

	}
}
