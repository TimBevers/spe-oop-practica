public class Main {

	public static void main(String[] args) {
		Breuk Een = new Breuk(2,4);
		Breuk Twee = new Breuk(-3,4);
		
		System.out.println("Een");
		System.out.println(Een);
		System.out.println(Een.Rationeel());
		
		System.out.println("Twee");
		System.out.println(Twee);
		System.out.println(Twee.Rationeel());
		
		System.out.println("Som");
		System.out.println(Een.Plus(Twee));
		System.out.println(Een.Plus(Twee).Rationeel());
		
		System.out.println("Vermenigvuldiging");
		System.out.println(Een.Vermenigvuldig(Twee));
		System.out.println(Een.Vermenigvuldig(Twee).Rationeel());
		
		
		Breuk EenOfNietEen = Een;
		System.out.println("Copy?");
		System.out.println(Een);
		System.out.println(EenOfNietEen);
		System.out.println("Eerste breuk -> 2/2");
		Een.setTeller(2);
		System.out.println(Een);
		System.out.println(EenOfNietEen);
	}

}
