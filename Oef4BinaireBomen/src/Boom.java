/*
 * Bevers Tim
 */
public class Boom {

	private Knoop wortel;

	public Boom(Knoop wortel) {
		super();
		this.wortel = wortel;
	}

	public int aantalKnopen() {
		return wortel.aantal();
	}

	public int knopenSom() {
		return wortel.som();
	}

	public boolean bevat(int i) {
		return (wortel.bevat(i) > 0);
	}

	@Override
	public String toString() {
		return wortel.toString();
	}

}
