public class Test {

	public static void main(String[] args) {
		Boom boom = new Boom(new InterneKnoop(10, //
				new InterneKnoop(8, //
						new Blad(5), //
						new Blad(9)), //
				new InterneKnoop(11, //
						new Blad(10), //
						new InterneKnoop(14, //
								null, //
								new Blad(15)))));
		System.out.println(boom);
		System.out.println(boom.aantalKnopen());
		System.out.println(boom.knopenSom());
		System.out.println(boom.bevat(11));
		System.out.println(boom.bevat(12));

	}
}
