/*
 * Bevers Tim
 */
public class InterneKnoop extends Knoop {

	private Knoop linkerkind;
	private Knoop rechterkind;

	public InterneKnoop(int waarde) {
		super(waarde);
	}

	public Knoop getLinkerkind() {
		return linkerkind;
	}

	public void setLinkerkind(Knoop linkerkind) {
		this.linkerkind = linkerkind;
	}

	public Knoop getRechterkind() {
		return rechterkind;
	}

	public void setRechterkind(Knoop rechterkind) {
		this.rechterkind = rechterkind;
	}

	public InterneKnoop(int waarde, Knoop linkerkind, Knoop rechterkind) {
		super(waarde);
		this.linkerkind = linkerkind;
		this.rechterkind = rechterkind;
	}

	public String toIndentedString(int i) {
		// bereken indentaties, tabs (\t) in dit geval
		int indentAmount = i;
		String indentString = "";
		while (indentAmount > 0) {
			indentString += "\t";
			indentAmount--;
		}
		String leeg = "-Leeg Blad";
		return String.format("-Knoop %s:\n%s%s\n%s%s", getWaarde(), indentString,
				(getLinkerkind() != null) ? getLinkerkind().toIndentedString(i + 1) : leeg, indentString,
				(getRechterkind() != null) ? getRechterkind().toIndentedString(i + 1) : leeg);
	}

	@Override
	public int aantal() {
		int aantal = 1;
		aantal += (getLinkerkind() != null) ? getLinkerkind().aantal() : 0;
		aantal += (getRechterkind() != null) ? getRechterkind().aantal() : 0;
		return aantal;
	}

	@Override
	public int som() {
		int aantal = getWaarde();
		aantal += (getLinkerkind() != null) ? getLinkerkind().som() : 0;
		aantal += (getRechterkind() != null) ? getRechterkind().som() : 0;
		return aantal;
	}

	@Override
	public int bevat(int i) {
		int aantal = (i == getWaarde() ? 1 : 0);
		aantal += (getLinkerkind() != null) ? getLinkerkind().bevat(i) : 0;
		aantal += (getRechterkind() != null) ? getRechterkind().bevat(i) : 0;
		return aantal;
	}

}
