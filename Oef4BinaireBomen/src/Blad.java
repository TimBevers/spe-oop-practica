/*
 * Bevers Tim
 */
public class Blad extends Knoop {

	public Blad(int waarde) {
		super(waarde);
	}

	public String toIndentedString(int i) {
		return String.format("-Blad %s", getWaarde());
	}

	@Override
	public int aantal() {
		return 1;
	}

	@Override
	public int som() {
		return getWaarde();
	}

	@Override
	public int bevat(int i) {
		return (i == getWaarde() ? 1 : 0);
	}

}
