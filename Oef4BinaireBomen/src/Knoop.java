/*
 * Bevers Tim 
 */
public abstract class Knoop {

	private int waarde;

	public Knoop(int waarde) {
		this.waarde = waarde;
	}

	public int getWaarde() {
		return waarde;
	}

	public void setWaarde(int waarde) {
		this.waarde = waarde;
	}

	// Geeft het aantal knopen onder de knoop terug (inclusief zichzelf)
	public abstract int aantal();

	// Geeft de som van de waarden van de knopen terug (inclusief zichzelf)
	public abstract int som();

	// Geeft het aantal keer dat een waade voorkomt in de boom(inclusief zichzelf)
	public abstract int bevat(int i);

	@Override
	public String toString() {
		return this.toIndentedString(1);
	}

	// Standaard toString() syntax me plaats voor aantal indentaties
	public String toString(int i) {
		return this.toIndentedString(i);
	}

	// Indentatie methode, voegt telkens 1 indentatie toe
	public abstract String toIndentedString(int indents);

}
