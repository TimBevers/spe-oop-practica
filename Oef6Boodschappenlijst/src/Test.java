public class Test {

	public static void main(String[] args) {
		Boodschap een = new Boodschap("stuff and things", 3);
		Boodschap twee = new Boodschap("stuff", 500, "g");
		Boodschap drie = new Boodschap("things", (float)3.5);
		Boodschap vier = new Boodschap("special things", 3);

		Boodschappenlijst lijst = new Boodschappenlijst(een, twee, drie, vier);
		for (int i = 0; i < 20; i++) {
			System.out.println(lijst.volgende());
		}
	}

}
