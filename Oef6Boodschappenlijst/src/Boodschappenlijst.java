
/*
 * Bevers Tim
 */

import java.util.ArrayList;

/**
 * Klasse die een lijst met boodschappen op een gebruiksvriendelijke manier aanbied.
 * 
 * @author Tim
 * @see Boodschap
 */
public class Boodschappenlijst {

	private int lastItem = 0;
	private ArrayList<Boodschap> actieveLijst;
	private ArrayList<Boodschap> archiefLijst;

	/**
	 * Genereerd een boodschappenlijst met een variabel aantal boodschappen.
	 * (Kan ook een lege lijst zijn.)
	 *
	 * @return Een boodschappenlijst
	 * @param boodschappen
	 *            Array van boodschappen of verschillende boodschapen geschijden
	 *            als argument.
	 * @see Boodschap
	 */
	public Boodschappenlijst(Boodschap... boodschappen) {
		actieveLijst = new ArrayList<Boodschap>();
		archiefLijst = new ArrayList<Boodschap>();
		this.voegToe(boodschappen);
	}

	private ArrayList<Boodschap> getActieveLijst() {
		return actieveLijst;
	}

	private ArrayList<Boodschap> getArchiefLijst() {
		return archiefLijst;
	}

	/**
	 * Voegt een variabel aantal boodschappen toe achteraan aan de lijst.
	 * 
	 * @param boodschappen
	 *            Array van boodschappen of verschillende boodschapen geschijden
	 *            als argument.
	 * @see Boodschap
	 */
	public void voegToe(Boodschap... boodschappen) {
		for (Boodschap i : boodschappen) {
			getActieveLijst().add(i);
		}
	}

	/**
	 * Verplaatst een variabel aantal boodschappen naar het archief.
	 * 
	 * @param boodschappen
	 *            Array van boodschappen of verschillende boodschapen geschijden
	 *            als argument.
	 * @see Boodschap
	 */
	public void naarArchief(Boodschap... boodschappen) {
		for (Boodschap i : boodschappen) {
			getArchiefLijst().add(i);
			getActieveLijst().remove(i);
		}
	}

	/**
	 * Verwijderd een variabel aantal boodschappen uit het archief.
	 * 
	 * @param boodschappen
	 *            Array van boodschappen of verschillende boodschapen geschijden
	 *            als argument.
	 * @see Boodschap
	 */
	public void verwijderUitArchief(Boodschap... boodschappen) {
		for (Boodschap i : boodschappen) {
			getArchiefLijst().remove(i);
		}
	}

	// Geeft de Index van telkens het volgende item (in een loop)
	private int volgendeIndex() {
		return (lastItem >= getActieveLijst().size() - 1) ? lastItem = 0 : ++lastItem;
	}

	/**
	 * Geeft het volgende object in de boodschappenlijst terug.
	 *
	 * @return De volgende boodschap in de lijst.
	 * @see Boodschap
	 */
	public Boodschap volgende() {
		return getActieveLijst().get(volgendeIndex());
	}

}
