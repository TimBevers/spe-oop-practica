/*
 * Bevers Tim
 */

public abstract class GekleurdeBand {

	private  int waarde;
	private  String kleur;

	public GekleurdeBand(int waarde, String kleur) {
		this.waarde = waarde;
		this.kleur = kleur;
	}

	public int getalWaarde() {
		return this.waarde;
	}

	@Override
	public String toString() {
		return "GekleurdeBand [kleur=" + kleur + "]";
	}

	public String getKleur() {
		return this.kleur;
	}

	public static GekleurdeBand zetStringOm(String kleur) {

		GekleurdeBand[] banden = new GekleurdeBand[] { new ZwarteBand(),
				new BruineBand(), new RodeBand() };

		for (GekleurdeBand i : banden) {
			if (i.getKleur().toLowerCase().contains(kleur.toLowerCase())) {
				return i;
			}
		}
		return banden[0];
	}

}
