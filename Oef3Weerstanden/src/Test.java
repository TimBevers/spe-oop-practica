public class Test {

	public static void main(String[] args) {
		Weerstand een = new Weerstand(2);
		System.out.println(een.getWeerstand());
		System.out.println(een);
		System.out.println(een.stroom(5));
		System.out.println(een.spanning(1));

		Weerstand[] array = new Weerstand[] { een, new Weerstand(3) };
		SerieSchakeling sch = new SerieSchakeling(array);
		System.out.println(sch);
		sch.voegToe(new Weerstand(5));
		System.out.println(sch);
		System.out.println(sch.spanning(1));
		System.out.println(sch.vervangingsWeerstand());
		System.out.println(sch.stroom(5));

		RegelbareWeerstand eenR = new RegelbareWeerstand(2, 8);
		System.out.println(eenR);
		System.out.println(eenR.verhoog((float) 5.25));
				
		Weerstand peen = new Weerstand (3); 
		Weerstand ptwee = new Weerstand (2); 
		RegelbareWeerstand sdrie = new RegelbareWeerstand (5,100); 
		ParallelSchakeling psch = new ParallelSchakeling(new Weerstand[] {peen,ptwee});
		SerieSchakeling spsch = new SerieSchakeling(new Component[]{psch, sdrie});
		System.out.println(spsch);
		System.out.println(spsch.vervangingsWeerstand());
		System.out.println(spsch.stroom(5));
		
		

	}
}
