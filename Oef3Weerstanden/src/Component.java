/*
 * Bevers Tim
 */

public abstract class Component {

	public abstract float vervangingsWeerstand();

	public float spanning(float stroom) {
		return stroom * vervangingsWeerstand();
	}

	public float stroom(float spanning) {
		return spanning / vervangingsWeerstand();
	}
}
