/*
 * Bevers Tim
 */

public class SerieSchakeling extends Schakeling {


	public SerieSchakeling() {
		super();
	}

	public SerieSchakeling(Component[] inComponenten) {
		super(inComponenten);
	}

	@Override
	public String toString() {
		return "serie" + super.toString();
	}

	public float vervangingsWeerstand() {
		float som = 0;
		for (Component i : getComponenten()) {
			som += i.vervangingsWeerstand();
		}
		return som;
	}

}
