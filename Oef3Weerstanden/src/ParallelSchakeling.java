/*
 * Bevers Tim
 */

public class ParallelSchakeling extends Schakeling {

	public ParallelSchakeling() {
		super();
	}

	public ParallelSchakeling(Component[] inComponenten) {
		super(inComponenten);
	}

	@Override
	public String toString() {
		return "parallel" + super.toString();
	}

	public float vervangingsWeerstand() {
		float invSom = 0;
		for (Component i : getComponenten()) {
			invSom += 1 / i.vervangingsWeerstand();
		}
		return 1 / invSom;
	}

}
