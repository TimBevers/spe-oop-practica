/*
 * Bevers Tim
 */

import java.util.Arrays;

public abstract class Schakeling extends Component {

	private Component[] componenten;

	public Schakeling() {
		setComponenten(new Component[0]);
	}

	public Schakeling(Component[] inComponenten) {
		setComponenten(inComponenten);
	}

	public Component[] getComponenten() {
		return componenten;
	}

	public void setComponenten(Component[] inComponenten) {
		this.componenten = inComponenten;
	}

	@Override
	public String toString() {
		String result = "";
		for (Component i : getComponenten()) {
			result += i + "; ";
		}
		return "schakeling van "
				+ getComponenten().length
				+ ((getComponenten().length == 1) ? " component: "
						: " componenten: ") + result;
	}

	public void voegToe(Weerstand inR) {
		Component[] nieuw = Arrays.copyOf(getComponenten(),
				getComponenten().length + 1);
		nieuw[getComponenten().length] = inR;
		setComponenten(nieuw);
	}
}
