/*
 * Bevers Tim
 */
package boom;

public class Boom {

	private Knoop wortel;

	public Knoop getWortel() {
		return wortel;
	}

	public void setWortel(Knoop wortel) {
		this.wortel = wortel;
	}

	public Boom() {
	}

	public Boom(Knoop wortel) {
		this.wortel = wortel;
	}

	public int aantalKnopen() {
		return wortel.aantal();
	}

	public float knopenSom() {
		return wortel.som();
	}

	public boolean bevat(BoomWaarde i) {
		return wortel.bevat(i);
	}

	@Override
	public String toString() {

		return (getWortel() != null) ? wortel.toString() : "Lege Boom!";
	}

	public void voegKnoopToe(BoomWaarde waarde) {
		if (getWortel() == null) {
			setWortel(new Blad(waarde));
		} else {
			setWortel(getWortel().voegToe(waarde));
		}

	}

}
