/*
 * Bevers Tim
 */

package boom;

public class InterneKnoop extends Knoop {

	private Knoop linkerkind;
	private Knoop rechterkind;

	public InterneKnoop(BoomWaarde waarde) {
		super(waarde);
	}

	public Knoop getLinkerkind() {
		return linkerkind;
	}

	public void setLinkerkind(Knoop linkerkind) {
		this.linkerkind = linkerkind;
	}

	public Knoop getRechterkind() {
		return rechterkind;
	}

	public void setRechterkind(Knoop rechterkind) {
		this.rechterkind = rechterkind;
	}

	public InterneKnoop(BoomWaarde waarde, Knoop linkerkind, Knoop rechterkind) {
		super(waarde);
		this.linkerkind = linkerkind;
		this.rechterkind = rechterkind;
	}

	public String toIndentedString(int i) {
		// bereken indentaties, tabs (\t) in dit geval
		int indentAmount = i;
		String indentString = "";
		while (indentAmount > 0) {
			indentString += "\t";
			indentAmount--;
		}
		String leeg = "-Leeg Blad";
		return String.format(
				"-Knoop %s:\n%s%s\n%s%s",
				getWaarde(),
				indentString,
				(getLinkerkind() != null) ? getLinkerkind().toIndentedString(
						i + 1) : leeg,
				indentString,
				(getRechterkind() != null) ? getRechterkind().toIndentedString(
						i + 1) : leeg);
	}

	@Override
	public int aantal() {
		int aantal = 1;
		aantal += (getLinkerkind() != null) ? getLinkerkind().aantal() : 0;
		aantal += (getRechterkind() != null) ? getRechterkind().aantal() : 0;
		return aantal;
	}

	@Override
	public float som() {
		float aantal = getWaarde().optelWaarde();
		aantal += (getLinkerkind() != null) ? getLinkerkind().som() : 0;
		aantal += (getRechterkind() != null) ? getRechterkind().som() : 0;
		return aantal;
	}

	@Override
	public boolean bevat(BoomWaarde i) {
		if (i.equals(getWaarde()))
			return true;

		if ((getLinkerkind() != null) ? getLinkerkind().bevat(i) : false)
			return true;

		if (i.lessthan(getWaarde())) // Efficient voor gesorteerde boom
			return false;

		if ((getRechterkind() != null) ? getRechterkind().bevat(i) : false)
			return true;

		return false;
	}

	@Override
	public Knoop voegToe(BoomWaarde waarde) {
		InterneKnoop knoop = new InterneKnoop(getWaarde(), getLinkerkind(),
				getRechterkind());
		if (waarde.lessthan(getWaarde())) {
			if (knoop.getLinkerkind() == null) {
				knoop.setLinkerkind(new Blad(waarde));
			} else {
				knoop.setLinkerkind(knoop.getLinkerkind().voegToe(waarde));
			}
		} else {
			if (knoop.getRechterkind() == null) {
				knoop.setRechterkind(new Blad(waarde));
			} else {
				knoop.setRechterkind(knoop.getRechterkind().voegToe(waarde));
			}
		}
		return knoop;
	}
}
