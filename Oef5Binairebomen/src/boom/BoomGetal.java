/*
 * Bevers Tim
 */

package boom;

public class BoomGetal implements BoomWaarde {

	private int waarde;

	public BoomGetal(int waarde) {
		super();
		this.waarde = waarde;
	}

	@Override
	public float optelWaarde() {
		return getWaarde();
	}

	public int getWaarde() {
		return waarde;
	}

	public void setWaarde(int waarde) {
		this.waarde = waarde;
	}

	@Override
	public boolean equals(BoomWaarde object) {
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (getClass() != object.getClass())
			return false;
		BoomGetal other = (BoomGetal) object;
		if (getWaarde() != other.getWaarde())
			return false;
		return true;
	}

	@Override
	public boolean lessthan(BoomWaarde object) {
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (getClass() != object.getClass())
			return false;
		BoomGetal other = (BoomGetal) object;
		if (getWaarde() >= other.getWaarde())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "" + getWaarde();
	}

}
