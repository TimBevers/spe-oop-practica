/*
 * Bevers Tim
 */

package boom;

public interface BoomWaarde {
	
	public float optelWaarde();
	public boolean equals(BoomWaarde object);
	public boolean lessthan(BoomWaarde object);
	public String toString();
	
}
