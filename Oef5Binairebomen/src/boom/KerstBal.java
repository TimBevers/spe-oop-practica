/*
 * Bevers Tim
 */

package boom;

import java.awt.Color;

public class KerstBal implements BoomWaarde {

	private int diameter;
	private Color kleur;

	public KerstBal(int waarde) {
		super();
		setDiameter(waarde);
		setKleur(Color.red);
	}
	
	public KerstBal(int waarde, Color color) {
		this(waarde);
		setKleur(color);
	}

	@Override
	public float optelWaarde() {
		return getVolume();
	}

	public int getDiameter() {
		return diameter;
	}

	public void setDiameter(int waarde) {
		this.diameter = waarde;
	}

	public Color getKleur() {
		return kleur;
	}

	public void setKleur(Color kleur) {
		this.kleur = kleur;
	}
	
	public float getVolume(){
		return (float)(Math.PI*Math.pow(getDiameter(), 3))/6;
	}

	@Override
	public boolean equals(BoomWaarde object) {
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (getClass() != object.getClass())
			return false;
		KerstBal other = (KerstBal) object;
		if (getVolume() != other.getVolume() || getKleur() != other.getKleur())
			return false;
		return true;
	}

	@Override
	public boolean lessthan(BoomWaarde object) {
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (getClass() != object.getClass())
			return false;
		KerstBal other = (KerstBal) object;
		if (getDiameter() >= other.getDiameter())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%s %s", getDiameter(), getKleur().toString());
	}

}
