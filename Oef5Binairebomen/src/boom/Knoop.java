/*
 * Bevers Tim 
 */
package boom;

public abstract class Knoop {

	private BoomWaarde waarde;

	public Knoop(BoomWaarde waarde) {
		this.waarde = waarde;
	}

	public BoomWaarde getWaarde() {
		return waarde;
	}

	public void setWaarde(BoomWaarde waarde) {
		this.waarde = waarde;
	}

	// Geeft het aantal knopen onder de knoop terug (inclusief zichzelf)
	public abstract int aantal();

	// Geeft de som van de waarden van de knopen terug (inclusief zichzelf)
	public abstract float som();

	// Waar al de waarde in de boom voorkomt(inclusief zichzelf)
	public abstract boolean bevat(BoomWaarde i);

	@Override
	public String toString() {
		return this.toIndentedString(1);
	}

	// Standaard toString() syntax me plaats voor aantal indentaties
	public String toString(int i) {
		return this.toIndentedString(i);
	}

	// Indentatie methode, voegt telkens 1 indentatie toe
	public abstract String toIndentedString(int indents);
	
	// Voegt een knoop toe aan een andere knoop
	public abstract Knoop voegToe(BoomWaarde waarde);
}
