/*
 * Bevers Tim
 */

package boom;

public class Blad extends Knoop {

	public Blad(BoomWaarde waarde) {
		super(waarde);
	}

	public String toIndentedString(int i) {
		return String.format("-Blad %s", getWaarde());
	}

	@Override
	public int aantal() {
		return 1;
	}

	@Override
	public float som() {
		return getWaarde().optelWaarde();
	}

	@Override
	public boolean bevat(BoomWaarde i) {
		return this.equals(i);
	}

	@Override
	public Knoop voegToe(BoomWaarde waarde) {
		InterneKnoop knoop = new InterneKnoop(getWaarde());
		if (waarde.lessthan(getWaarde())) {
			knoop.setLinkerkind(new Blad(waarde));
		} else {
			knoop.setRechterkind(new Blad(waarde));
		}
		return knoop;
	}

}
