import boom.*;

public class Test {

	public static void main(String[] args) {
		Boom boom = new Boom(new InterneKnoop(new BoomGetal(10), //
				new InterneKnoop(new BoomGetal(8), //
						new Blad(new BoomGetal(5)), //
						new Blad(new BoomGetal(9))), //
				new InterneKnoop(new BoomGetal(11), //
						new Blad(new BoomGetal(10)), //
						new InterneKnoop(new BoomGetal(14), //
								null, //
								new Blad(new BoomGetal(15))))));
		System.out.println(boom);
		System.out.println(boom.aantalKnopen());
		System.out.println(boom.knopenSom());
		System.out.println(boom.bevat(new BoomGetal(11)));
		System.out.println(boom.bevat(new BoomGetal(12)));
		
		Boom kerstboom = new Boom(new InterneKnoop(new KerstBal(10), //
				new InterneKnoop(new KerstBal(8), //
						new Blad(new KerstBal(5)), //
						new Blad(new KerstBal(9))), //
				new InterneKnoop(new KerstBal(11), //
						new Blad(new KerstBal(10)), //
						new InterneKnoop(new KerstBal(14), //
								null, //
								new Blad(new KerstBal(15))))));
		System.out.println(kerstboom);
		System.out.println(kerstboom.aantalKnopen());
		System.out.println(kerstboom.knopenSom());
		System.out.println(kerstboom.bevat(new KerstBal(11)));
		System.out.println(kerstboom.bevat(new KerstBal(12)));

		Boom startboom = new Boom();
		System.out.println(startboom);
		startboom.voegKnoopToe(new BoomGetal(10));
		startboom.voegKnoopToe(new BoomGetal(8));
		startboom.voegKnoopToe(new BoomGetal(11));
		startboom.voegKnoopToe(new BoomGetal(5));
		startboom.voegKnoopToe(new BoomGetal(9));
		startboom.voegKnoopToe(new BoomGetal(10));
		startboom.voegKnoopToe(new BoomGetal(14));
		startboom.voegKnoopToe(new BoomGetal(15));
		System.out.println(startboom);

	}
}
