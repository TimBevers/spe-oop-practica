package view;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

@SuppressWarnings("serial")
public class RichtingButtons extends JPanel {

	private JLabel label = new JLabel("", SwingConstants.CENTER);
	private JButton up = new JButton("▲");
	private JButton down = new JButton("▼");
	private JButton left = new JButton("◄");
	private JButton right = new JButton("►");

	public RichtingButtons() {
		this.setLayout(new BorderLayout());
		this.add(label, BorderLayout.CENTER);
		this.add(up, BorderLayout.PAGE_START);
		this.add(down, BorderLayout.PAGE_END);
		this.add(left, BorderLayout.LINE_START);
		this.add(right, BorderLayout.LINE_END);
	}

	public void setText(String s) {
		label.setText(s);
	}
	
	public void upEventListener(ActionListener a){
		up.addActionListener(a);
	}
	
	public void downEventListener(ActionListener a){
		down.addActionListener(a);
	}
	
	public void leftEventListener(ActionListener a){
		left.addActionListener(a);
	}
	
	public void rightEventListener(ActionListener a){
		right.addActionListener(a);
	}
}
