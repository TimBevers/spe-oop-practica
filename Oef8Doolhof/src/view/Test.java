/*
 * Bevers Tim
 */
package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//import controler.*;
import model.*;

public class Test extends JFrame {

	private final int SIZE = 4;

	public static void main(String[] args) {
		new Test();
	}

	/**
	 * Default serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	// Input van boodschappen
	// Aantal

	Container c = getContentPane();
	DoolHof d = new DoolHof(SIZE);
	RichtingButtons rb = new RichtingButtons();
	Component dh = new JPanel();

	public Test() {
		// Titel van het venster
		super("Doolhof Test");

		c.setLayout(new BorderLayout());
		fillDoolHof();
		c.add(dh, BorderLayout.CENTER);
		c.add(rb, BorderLayout.PAGE_END);
		addActionlisterners();

		Dimension size = new Dimension(300, 400);
		setMinimumSize(size);
		Dimension prefsize = new Dimension(80 * SIZE, 80 * SIZE);
		setPreferredSize(prefsize);

		// Display the window.
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void fillDoolHof() {
		if (dh.getParent() == c) {
			c.remove(dh);
		}
		JPanel p = new JPanel();
		GridLayout gl = new GridLayout(2 * SIZE, 2 * SIZE);
		p.setLayout(gl);
		for (int i = 0; i < (2 * SIZE); i++) {
			for (int j = 0; j < (2 * SIZE); j++) {
				JPanel mj = new JPanel();
				mj.setLayout(new GridLayout(3, 3));
				for (int k = 0; k < 3; k++) {
					for (int l = 0; l < 3; l++) {
						Gebied g = d.getDoolhof()[j][i].getGebieden()[l][k];
						JLabel il = new JLabel(g.toString(), SwingConstants.CENTER);
						if (g.isMuur()) {
							il.setBackground(Color.BLACK);
							il.setOpaque(true);
							// il.setText(i + "" + j + "" + l + "" + k);
						}
						mj.add(il);
					}
				}
				p.add(mj);
			}
		}
		dh = p;
		c.add(dh, BorderLayout.CENTER);
		c.revalidate();
		c.repaint();
	}

	private void showWin(){
		JOptionPane.showMessageDialog(getContentPane(),
				  "Je hebt het goud gevonden.", "Grats",
				  JOptionPane.PLAIN_MESSAGE);
	}
	
	private void addActionlisterners() {
		rb.upEventListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(d.verplaatsSpeler(Move.UP)){
						showWin();
					}
					fillDoolHof();
				} catch (InvalidMoveException e) {
					rb.setText(e.getMessage());
				}
			}
		});
		rb.downEventListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(d.verplaatsSpeler(Move.DOWN)){
						showWin();
					}
					fillDoolHof();
				} catch (InvalidMoveException e) {
					rb.setText(e.getMessage());
				}
			}
		});
		rb.leftEventListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(d.verplaatsSpeler(Move.LEFT)){
						showWin();
					}
					fillDoolHof();
				} catch (InvalidMoveException e) {
					rb.setText(e.getMessage());
				}
			}
		});
		rb.rightEventListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(d.verplaatsSpeler(Move.RIGHT)){
						showWin();
					}
					fillDoolHof();
				} catch (InvalidMoveException e) {
					rb.setText(e.getMessage());
				}
			}
		});
	}

}
