/*
 * Bevers Tim
 */

package model;

public class Gebied {

	public static final int LEEG = 0;
	public static final int MUUR = 1;
	public static final int SPELER = 2;
	public static final int GOUD = 3;

	private int inhoud;

	public Gebied(int inhoud) {
		setInhoud(inhoud);
	}

	private void setInhoud(int inhoud) {
		this.inhoud = inhoud;
	}

	private int getInhoud() {
		return inhoud;
	}

	public boolean isLeeg() {
		return (getInhoud() == Gebied.LEEG);
	}

	public boolean isMuur() {
		return (getInhoud() == Gebied.MUUR);
	}

	public boolean isSpeler() {
		return (getInhoud() == Gebied.SPELER);
	}
	
	public void verwijderSpeler() {
		setInhoud(Gebied.LEEG);
	}
	
	public void plaatsSpeler() {
		setInhoud(Gebied.SPELER);
	}

	public void setSpecial(int g) {
		setInhoud(g);
	}

	public boolean isGoud() {
		return (getInhoud() == Gebied.GOUD);
	}

	@Override
	public String toString() {
		switch (this.getInhoud()){
		case Gebied.MUUR:
			return "#";
		case Gebied.SPELER:
			return "x";
		case Gebied.GOUD:
			return "G";
		default:
			return " ";
		}
	}

}
