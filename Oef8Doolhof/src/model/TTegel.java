/*
 * Bevers Tim
 */

package model;

public class TTegel extends Tegel {
	public TTegel(){
		setGebieden(new Gebied[][] {
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.LEEG),	new Gebied(Gebied.MUUR) },
				{ new Gebied(Gebied.LEEG), new Gebied(Gebied.LEEG),	new Gebied(Gebied.LEEG) },
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.MUUR),	new Gebied(Gebied.MUUR) } });
	}

}
