/*
 * Bevers Tim
 */

package model;

public class Coord {
	private int x;
	private int y;

	public Coord() {
	}

	public Coord(int ix, int iy) {
		this();
		setX(ix);
		setY(iy);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Coord centerInTile() {
		Coord c = new Coord();
		c.setX(this.getX() - this.getX() % 3 + 1);
		c.setY(this.getY() - this.getY() % 3 + 1);
		return c;
	}

	public static Coord random(int size) {
		return new Coord((int) ((Math.random() * 3 * 2 * size)), (int) ((Math.random() * 3 * 2 * size)));
	}

	public Coord move(Move m, int size) throws InvalidMoveException {
		switch (m) {
		case UP:
			if (this.getY() - 1 < 0) {
				throw new InvalidMoveException();
			} else {
				return new Coord(this.getX(), this.getY() - 1);
			}
		case DOWN:
			if (this.getY() + 1 >= 2 * size * 3) {
				throw new InvalidMoveException();
			} else {
				return new Coord(this.getX(), this.getY() + 1);
			}
		case LEFT:
			if (this.getX() - 1 < 0) {
				throw new InvalidMoveException();
			} else {
				return new Coord(this.getX() - 1, this.getY());
			}
		case RIGHT:
			if (this.getX() + 1 >= 2 * size * 3) {
				throw new InvalidMoveException();
			} else {
				return new Coord(this.getX() + 1, this.getY());
			}
		default:
			return this;
		}
	}

	@Override
	public String toString() {
		return "( " + x + " , " + y + " )";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coord other = (Coord) obj;
		if (this.getX() != other.getX())
			return false;
		if (this.getY() != other.getY())
			return false;
		return true;
	}
}
