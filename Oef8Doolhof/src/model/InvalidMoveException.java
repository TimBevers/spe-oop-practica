/*
 * Bevers Tim
 */

package model;

public class InvalidMoveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMoveException() {
	}

	public InvalidMoveException(String s) {
		super(s);
	}
}
