/*
 * Bevers Tim
 */

package model;

import java.util.ArrayList;
import java.util.Collections;

public class DoolHof {

	private Tegel[][] doolhof;

	private Coord speler = new Coord();

	private int size = 0;

	public DoolHof(int size) {
		this.setSize(size);
		this.setDoolhof(this.maakDoolhof(getSize()));
		this.plaats(Gebied.SPELER);
		this.plaats(Gebied.GOUD);
	}

	public Tegel[][] getDoolhof() {
		return doolhof;
	}

	public void setDoolhof(Tegel[][] doolhof) {
		this.doolhof = doolhof;
	}

	private Coord getSpeler() {
		return speler;
	}

	private void setSpeler(Coord speler) {
		this.speler = speler;
	}

	public int getSize() {
		return size;
	}

	private void setSize(int size) {
		this.size = size;
	}

	private Tegel getTegel(Coord c) {
		return getDoolhof()[c.getX() / 3][c.getY() / 3];
	}

	private Tegel[][] maakDoolhof(int n) {
		Tegel[][] doolhof = new Tegel[2 * n][2 * n];
		ArrayList<Tegel> l = new ArrayList<Tegel>();
		for (int i = 0; i < n * n; i++) {
			l.add(new CrossTegel());
			l.add(new TTegel());
			l.add(new StraightTegel());
			l.add(new BendTegel());
		}
		Collections.shuffle(l);
		for (int i = 0; i < 2 * n; i++) {
			for (int j = 0; j < 2 * n; j++) {
				int rnd = (int) (Math.random() * 4);
				for (int k = 0; k < rnd; k++) {
					l.get(0).draai();
				}
				doolhof[i][j] = l.remove(0);
			}
		}
		return doolhof;
	}

	private void plaats(int g) {
		boolean ok = false;
		do {
			Coord c = Coord.random(getSize());
			Tegel t = this.getTegel(c);
			ok = t.plaatsSpecial(g);
			if (g == Gebied.SPELER){
				this.setSpeler(c.centerInTile());
			}
		} while (!ok);
	}

	public boolean verplaatsSpeler(Move m) throws InvalidMoveException {
		try {
			Coord n = this.getSpeler().move(m, this.getSize());
			Tegel t = this.getTegel(n);
			t.plaatsSpeler(n);
			this.getTegel(this.getSpeler()).verwijderSpeler(this.getSpeler());
			this.setSpeler(n);
			return t.bevatGoud();

		} catch (InvalidMoveException e) {
			System.out.println("Invalid Move");
			throw new InvalidMoveException("Er staat een muur in de weg!");
		}
	}

	@Override
	public String toString() {
		String s = "";
		for (Tegel[] d : doolhof) {
			for (Tegel dd : d) {
				s += dd;
			}
		}
		return s;
	}

}
