/*
 * Bevers Tim
 */

package model;

public class CrossTegel extends Tegel {

	public CrossTegel() {
		setGebieden(new Gebied[][] {
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.LEEG),	new Gebied(Gebied.MUUR) },
				{ new Gebied(Gebied.LEEG), new Gebied(Gebied.LEEG),	new Gebied(Gebied.LEEG) },
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.LEEG),	new Gebied(Gebied.MUUR) } });
	}

}
