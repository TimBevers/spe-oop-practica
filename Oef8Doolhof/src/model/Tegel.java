/*
 * Bevers Tim
 */

package model;

public abstract class Tegel {

	private Gebied[][] gebieden;

	public Gebied[][] getGebieden() {
		return gebieden;
	}

	public void setGebieden(Gebied[][] gebieden) {
		this.gebieden = gebieden;
	}

	public void draai() {
		Gebied[][] t = new Gebied[3][3];
		int n = 3;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				t[i][j] = getGebieden()[n - 1 - j][i];
			}
		}
		setGebieden(t);
	}

	public boolean plaatsSpecial(int g) {
		if (gebieden[1][1].isLeeg()) {
			gebieden[1][1].setSpecial(g);
			return true;
		}
		return false;
	}

	public boolean bevatGoud() {
		return gebieden[1][1].isGoud();
	}

	public void verwijderSpeler(Coord c) {
		this.getGebieden()[c.getX() % 3][c.getY() % 3].verwijderSpeler();
	}

	public void plaatsSpeler(Coord c) throws InvalidMoveException {
		Gebied g = this.getGebieden()[c.getX() % 3][c.getY() % 3];
		if (g.isMuur())
			throw new InvalidMoveException();
		g.plaatsSpeler();
	}

	@Override
	public String toString() {
		String s = "";
		for (Gebied[] r : getGebieden()) {
			for (Gebied g : r) {
				s += g.toString();
			}
			s += "\n";
		}
		return s;
	}

}
