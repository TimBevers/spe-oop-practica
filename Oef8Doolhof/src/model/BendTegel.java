/*
 * Bevers Tim
 */

package model;

public class BendTegel extends Tegel{
	public BendTegel(){
		setGebieden(new Gebied[][] {
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.LEEG),	new Gebied(Gebied.MUUR) },
				{ new Gebied(Gebied.LEEG), new Gebied(Gebied.LEEG),	new Gebied(Gebied.MUUR) },
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.MUUR),	new Gebied(Gebied.MUUR) } });
	}
}
