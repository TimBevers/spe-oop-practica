/*
 * Bevers Tim
 */

package model;

public class StraightTegel extends Tegel{
	public StraightTegel(){
		setGebieden(new Gebied[][] {
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.MUUR),	new Gebied(Gebied.MUUR) },
				{ new Gebied(Gebied.LEEG), new Gebied(Gebied.LEEG),	new Gebied(Gebied.LEEG) },
				{ new Gebied(Gebied.MUUR), new Gebied(Gebied.MUUR),	new Gebied(Gebied.MUUR) } });
	}

}
