/*
 * Bevers Tim
 */

package controler;

import model.*;
import view.*;

public class Controller {

	public static void main(String[] args) {
		new Controller();
	}

	private GUI gui;
	private Boodschappenlijst bLijst;

	public Controller() {
		gui = new GUI(this);
		bLijst = new Boodschappenlijst();
	}

	public void voegBoodschapenToe(Boodschap... bn) {
		bLijst.voegToe(bn);
	}

	public Boodschap getVolgendeBoodschap() {
		return bLijst.volgende();
	}

}
