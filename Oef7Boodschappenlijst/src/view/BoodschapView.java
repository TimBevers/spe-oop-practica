/*
 * Bevers Tim
 */

package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import model.*;

@SuppressWarnings("serial")
public class BoodschapView extends JPanel {

	public BoodschapView() {
		this.setLayout(new BorderLayout());
		Container text = new JTextArea();
		Container scroll = new JScrollPane(text);
		this.add(scroll, BorderLayout.CENTER);
		
		setVisible(true);
	}
}
