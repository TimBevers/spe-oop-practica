/*
 * Bevers Tim
 */
package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import controler.*;
import model.*;

public class GUI extends JFrame {

	/**
	 * Default serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	Controller con;

	// Input van boodschappen
	// Aantal
	JLabel labelAantal = new JLabel("Aantal");
	JTextField textAantal = new JTextField();

	// Omschrijving
	JLabel labelOmschrijving = new JLabel("Omschrijving");
	JTextField textOmschrijving = new JTextField();

	// Buttons
	JButton buttonVoegToe = new JButton("Voeg Toe");
	JButton buttonVolgende = new JButton("Volgende");

	// Weergeven van boodschappen
	JLabel boodschap = new JLabel("Welkom");

	public GUI() {
		// Titel van het venster
		super("Boodchappenlijst");

		fillPane();
		addActionlisterners();

		Dimension size = new Dimension(300, 200);
		setMinimumSize(size);

		// Display the window.
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public GUI(Controller c) {
		this();
		this.con = c;
	}

	private void fillPane() {
		Container c = getContentPane();
		GridBagLayout gbl = new GridBagLayout();
		c.setLayout(gbl);
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 0.5;
		gbc.weightx = 0.5;
		gbc.gridy = 0;
		gbc.gridx = 0;
		c.add(labelAantal, gbc);
		gbc.gridy = 0;
		gbc.gridx = 1;
		c.add(textAantal, gbc);
		gbc.gridy = 1;
		gbc.gridx = 0;
		c.add(labelOmschrijving, gbc);
		gbc.gridy = 1;
		gbc.gridx = 1;
		c.add(textOmschrijving, gbc);
		gbc.gridy = 2;
		gbc.gridx = 0;
		c.add(buttonVoegToe, gbc);
		gbc.gridy = 2;
		gbc.gridx = 1;
		c.add(buttonVolgende, gbc);
		gbc.gridy = 3;
		gbc.gridx = 0;
		c.add(boodschap, gbc);
		gbc.gridy = 4;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		c.add(new BoodschapView(), gbc);

	}

	private void addActionlisterners() {
		this.buttonVoegToe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					float aantal = Float.parseFloat(textAantal.getText());
					Boodschap b = new Boodschap(textOmschrijving.getText(),
							aantal);
					con.voegBoodschapenToe(b);
				} catch (NullPointerException e) {

				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(getContentPane(),
							"Het aantal moet een getal zijn.", "Input Error",
							JOptionPane.WARNING_MESSAGE);
				} catch (TeWeinigException e) {
					JOptionPane.showMessageDialog(getContentPane(),
							"Het aantal moet groter dan 0 zijn.",
							"Input Error", JOptionPane.WARNING_MESSAGE);
				}
			}

		});

		this.buttonVolgende.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Boodschap b = con.getVolgendeBoodschap();
					boodschap.setText(b.toString());
				} catch (NullPointerException e) {
				}
			}

		});
	}

}
