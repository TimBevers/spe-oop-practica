/*
 * Bevers Tim
 */
package model;

import java.text.DecimalFormat;

/**
 * Klasse die een enkele boodschap omschrijft d.m.v. een omschrijving, eenheid
 * (optioneel) en een aantal.
 * 
 * @author Tim
 * @see Boodschappenlijst
 * 
 */
public class Boodschap {
	private String omschrijving;
	private float aantal;
	private String eenheid;

	public String getOmschrijving() {
		return omschrijving;
	}

	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public float getAantal() {
		return aantal;
	}

	public void setAantal(float aantal) {
		this.aantal = aantal;
	}

	public String getEenheid() {
		return eenheid;
	}

	public void setEenheid(String eenheid) {
		this.eenheid = eenheid;
	}

	public Boodschap(String omschrijving, float aantal) {
		this.omschrijving = omschrijving;
		this.aantal = aantal;
	}

	public Boodschap(String omschrijving, float aantal, String eenheid) {
		this(omschrijving, aantal);
		this.eenheid = eenheid;
	}

	public static boolean checkBoodschap(Boodschap b) {
		if (b.aantal <= 0)
			return true;
		return false;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.##");
		return (getEenheid() == null) ? String.format("%s x %s",
				df.format(getAantal()), getOmschrijving()) : String.format(
				"%s%s %s", df.format(getAantal()), getEenheid(),
				getOmschrijving());
	}

}
